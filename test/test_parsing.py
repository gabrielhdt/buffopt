import unittest

import _context
from buffopt.constraintparse import lexer, parser
from buffopt.constraintparse.cstTypes import (ConstraintEq,
                                              VarSpec)


class ParserTest(unittest.TestCase):
    def test_lhident(self):
        """Left hand side identifier."""
        env = {}
        lex = lexer.gen_lexer(env)
        cstr = "{0} <= 1."
        res = parser.parse(cstr, lex)
        self.assertEqual(res, [ConstraintEq([VarSpec(0, 1.)], 1.)])

    def test_lhfact(self):
        """Left hand side factors."""
        env = {}
        lex = lexer.gen_lexer(env)
        cstr = "3. * {0} <= 1."
        res = parser.parse(cstr, lex)
        self.assertEqual(res, [ConstraintEq([VarSpec(0, 3.)], 1.)])

    def test_lhcombine(self):
        """Left hand side combination."""
        env = {}
        lex = lexer.gen_lexer(env)
        cstr = "1. * {0} + 1. * {1} <= 1."
        resp = parser.parse(cstr, lex)

        cstrm = "1. * {0} - 1. * {1} <= 1."
        resm = parser.parse(cstrm, lex)

        self.assertEqual(
            resp,
            [ConstraintEq([VarSpec(0, 1.), VarSpec(1, 1.)], 1.)])
        self.assertEqual(
            resm,
            [ConstraintEq([VarSpec(0, 1.), VarSpec(1, -1.)], 1.)])

    def test_uminus(self):
        """Unary minus."""
        env = {}
        lex = lexer.gen_lexer(env)
        cstr = "~-1. * {0} <= 1."
        res = parser.parse(cstr, lex)
        self.assertEqual(res, [ConstraintEq([VarSpec(0, -1.)], 1.)])

    def test_rhreduce(self):
        env = {}
        lex = lexer.gen_lexer(env)
        cstr = """{0} <= 3. + 2."""
        res = parser.parse(cstr, lex)
        self.assertEqual(res, [ConstraintEq([VarSpec(0, 1.)], 5.)])

    def test_env_var(self):
        env = {"v": 2.}
        lex = lexer.gen_lexer(env)
        cstr = """{0} <= 3. + v"""
        res = parser.parse(cstr, lex)
        self.assertEqual(res, [ConstraintEq([VarSpec(0, 1.)], 5.)])

    def test_env_arr(self):
        env = {"v": [0, 1, 2]}
        lex = lexer.gen_lexer(env)
        cstr = """{0} <= 3. + v[2]"""
        res = parser.parse(cstr, lex)
        self.assertEqual(res, [ConstraintEq([VarSpec(0, 1.)], 5.)])
