import unittest

import _context
from buffopt import schedule
from buffopt import simulator as strategic
from buffopt.proba_distrib import non_reactionary_delay


class SimulatorTest(unittest.TestCase):
    def setUp(self):
        self.csched = schedule.make(*_context.csched_data)

    def test_non_reac_delay(self):
        rd_delay = non_reactionary_delay()
        self.assertIsInstance(rd_delay, int)

    def test_run(self):
        legsdel = strategic.run(self.csched, _context.shifts)
        self.assertIsInstance(legsdel, list)
