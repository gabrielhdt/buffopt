import functools
import random
import unittest

import _context
from buffopt import schedule, geneticsolver


class GeneticSolverTest(unittest.TestCase):
    def setUp(self):
        self.csched = schedule.make(*_context.csched_data)

    def test_init_pop(self):
        ps = 10
        ipop = geneticsolver.make(ps, self.csched)
        self.assertEqual(len(ipop), ps)
        # self.assertEqual(len(ipop[0][0]), gs)

    def test_roulette_wheel(self):
        """Testing the roulette selection.

        The principle of the roulette wheel selection is that the
        probability that an element is selected should be proportional
        to its fitness associated.
        """
        chromfit = [(0, 0.6), (1, 0.3), (2, 0.1)]
        selections = [
            geneticsolver._roulette_wheel(chromfit) for _ in range(100)
        ]

        # verify the distributions
        def count_chrom(c, s):
            return functools.reduce(
                lambda acc, ic: acc + 1 if ic == c else acc, s, 0)

        cc0, cc1, cc2 = [count_chrom(c, selections) for c in [0, 1, 2]]
        self.assertAlmostEqual(cc0, 60, delta=10)

    def test_one_point_cross(self):
        [c1, c2] = [geneticsolver._rand_chromosome(5) for _ in range(2)]
        c3, c4 = geneticsolver._one_point_cross(c1, c2)
        self.assertNotEqual(c1[0], c2[0])

    def test_evolve(self):
        """Ensure an evolution brings better chromosomes.

        In all cases, best of a new pop should be geq than best of
        previous pop.
        """
        ipop = geneticsolver.make(10, self.csched)
        ielite = geneticsolver.make_elite(5, ipop, self.csched)[:2]
        mutation = 10

        impop, imelite = geneticsolver.evolve(self.csched, ipop, ielite,
                                              mutation)
        bestfit = max([geneticsolver.fitness(self.csched, c[0], 2, 0.8)
                       for c in impop])
        bestelfit = max([geneticsolver.fitness(self.csched, e[0], 20, 0.8)
                         for e in imelite])
        nimpop, nimelite = geneticsolver.evolve(self.csched, impop, imelite,
                                                mutation)
        nbestfit = max([geneticsolver.fitness(self.csched, c[0], 2, 0.8)
                        for c in nimpop])
        nbestelfit = max([geneticsolver.fitness(self.csched, e[0], 20, 0.8)
                          for e in nimelite])
        self.assertGreaterEqual(nbestfit, bestfit)
        self.assertGreaterEqual(nbestelfit, bestelfit)

    def test_mutate(self):
        c = [random.randint(0, 20) for _ in range(1000)]
        mutation = 10
        nc = geneticsolver._mutate(c, mutation)
        countchanged = functools.reduce(lambda acc, b: acc + 1 if b else acc,
                                        [g != ng for g, ng in zip(c, nc)], 0)
        self.assertAlmostEqual(
            countchanged,
            geneticsolver._k_mutate_p * len(c),
            delta=0.1 * len(c))
