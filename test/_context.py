import json
import os
import sys
import tempfile

sys.path.insert(0,
                os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

init_schedule = [6 * 60, 9 * 60, 12 * 60]
"""Initial schedule"""
block_times = [70, 90, 90]
"""Durations of legs"""
t_turnover = 20
"""Duration of aircraft turnover"""
env = {"t_turnover": t_turnover}
"""Environment to pass to lexer"""
constraints = """
{0} + ~-1. * {1} <= ~-1. * block_times[0] - t_turnover
{1} + ~-1. * {2} <= ~-1. * block_times[1] - t_turnover
""".strip()
shifts = [-3, 6, 8]
"""example shifts"""
csched_data = (init_schedule, block_times, constraints, 72000, "medium", env)
"""Data used to make a schedule in other test modules"""

tmpdir = tempfile.mkdtemp()
"""A temporary directory"""
_situation = {
    "aircraft": "a320",
    "legs": [
        {"t": 362, "airport": "lfdl"},
        {"t": 480, "airport": "lfbo"},
        {"t": 660, "airport": "lfdl"},
        {"t": None, "airport": "lfbo"}
    ]
}
jsituation = json.dumps(_situation)
