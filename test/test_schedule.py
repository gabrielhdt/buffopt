# -*- coding: utf-8 -*-
import unittest

import _context
from buffopt import schedule


class ConstScheduleTest(unittest.TestCase):
    def setUp(self):
        self.schedule = schedule.make(*_context.csched_data)

    def test_make(self):
        pass

    def test_correct(self):
        """Correction behaviour.

        Checks that shifts violating constraints are corrected and that
        shifts not violating any constraints are not.
        """
        probshift = [2 * 60, 0, 0]
        newshift = schedule.correct(self.schedule, probshift)
        self.assertNotEqual(newshift, probshift)
        renewshifts = schedule.correct(self.schedule, newshift)
        self.assertEqual(renewshifts, newshift)

    def test_check(self):
        """Checks that schedule are well checked."""
        self.assertTrue(schedule.check(self.schedule)[0])

        isched = [6 * 60, 8 * 60]
        block_times = [70, 80]
        mtow = 80000
        scenario = "low"
        cstrs = """
        {0} - {1} <= ~-1. * block_times[0] - 120.
        {1} <= ~-1. * block_times[1] + 10. * 60.
        """.strip()
        fsched = schedule.make(isched, block_times, cstrs, mtow, scenario)
        result = schedule.check(fsched)
        self.assertFalse(result[0])
        self.assertEqual(result[1], [0])

    def test_delay_cost(self):
        pass

    def test_buffer_cost(self):
        shifts = [60, 60, 60]  # Three hours of buffer...
        cost = schedule.buffer_cost(self.schedule, shifts)
        self.assertLessEqual(cost, 1020 * 3)
        self.assertGreaterEqual(cost, 760 * 3)
