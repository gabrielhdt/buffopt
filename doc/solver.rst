Solver
======

Genetic algorithm
-----------------

Fitness function
  Let :math:`d` be a function giving an estimation of the delay.  Then
  with :math:`c` a chromosome and :math:`\Sigma` a schedule,
  :math:`d(c, \Sigma)` is an estimation of the delay created by
  application of shifts :math:`c` on schedule :math:`\Sigma`.
  Let :math:`s(c, \Sigma)` be the time lapse between the first
  departure and the last departure.  Let :math:`\alpha \in [0, 1]` be
  a coefficient.  The fitness function is then

  .. math::

    f(c, \Sigma) = \frac{1}{\alpha d(c, \Sigma) + (1 - \alpha) s(c,
    \Sigma)

Population management
  Cross over
    Two types of crossing are used,

    * arithmetic crossing,
    * one point cross over.

    The type of cross over is chosen randomly

  Elite population
    Elitism is used.  Once a generation is produced (cross overs and
    mutations have been applied), the best elements of this new population
    are mixed the elite population.  Then a higher number of simulations are
    carried out to have a more precise score for each elite individual.  The
    new elite population is extracted from the previously made mix.

Complexity
  Let :math:`p` be the number of individual in the population.  The `evolve`
  function is then in :math:`O(p^2)`.  Therefore, the algorithm is in
  :math:`O(np^2)`.

Results
-------

Generation number
  When fixing the population size, the fitness seems to grow
  proportionnaly to the number of the generation.  With an
  :math:`alpha` of 0.8, a linear regression gives a coefficient of
  :math:`3.115 \cdot 10^{-8}`.

Population size
  The evolution of the fitness in function of the population size might be
  analysed via a linear approximation even though the linearity of the data
  is far less obvious.
