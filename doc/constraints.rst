Constraints
===========

Modeling constraints
--------------------
Constraints are modeled as a set of equations. With the following
notations,

* :math:`C_p` set of flights requiring a passenger connection (set of
  tuples),
* :math:`C_c` set of flights requiring a crew connection,
* :math:`C_{A1}` set of aircraft connections without scheduled
  maintainance in between,
* :math:`C_{A2}` set of aircraft connections with scheduled
  maintainance in between,
* :math:`S` set of airports,
* :math:`L` set of legs,
* :math:`\tau_{B},i` the block time allowed for flight :math:`i`,
* :math:`\tau_{P_{conn}}` the time for the passengers to connect between
  two flights,
* :math:`\tau_{C_{conn}}` the time for the crew to connect between two
  flights,
* :math:`\tau_{A_t}` the time required for the turnover of the
  aircraft,
* :math:`T_{E,s}` minimal time of departure in airport :math:`s`,
* :math:`T_{L,s}` maximal time of arrival in airport :math:`s`

the constraints are

* :math:`\forall (i, j) \in C_p, x_i + \tau_{B,i} + \tau_{P_{conn}} \leq
  x_j`
* :math:`\forall (i, j) \in C_c, x_i + \tau_{B,i} + \tau_{C_{conn}}
  \leq x_j`
* :math:`\forall (i, j) \in C_{A1}, x_i + \tau_{B,i} + \tau_{Aturn}
  \leq x_j`
* :math:`\forall s \in S, \forall i \in L_D(s), x_i \geq T_{E,s}`
* :math:`\forall s \in S, \forall i \in L_A(s), x_i + \tau_{B,i} \leq
  T_{L,s}`

Bringing back solutions the feasible domain
-----------------------------------------------
Let :math:`n = |L|`.Consider constraint 1, it can be written as
:math:`A_1 x \leq - (\tau_{B,k} + \tau_{P_c})^\top_{k\in{1, \dots,
n}}`. To build :math:`A_1`, say :math:`|C_p| = c_p` and let for any
:math:`k \in [1, c_p] \cap \mathbb{N}`, :math:`C_{p,k}` be the
:math:`k` th element of :math:`C_p`. Then :math:`A_1 \in
\mathcal{M}_{c_p \times n}(\{0, 1\})` and by noting :math:`\forall k
\in [1, c_p] \cap \mathbb{N}, l_k^1` the :math:`k` th line of
:math:`A_1`, we have :math:`l_k^1 = [\mathbb{1}_{i \in
{C_{p,k}}\{i\}}(j)]_{j \in [1, n] \cap \mathbb{N}}`.


.. _constraints-input:

Constraints input
-----------------
Constraints are loaded in ``data.py``. The file ``data.py`` must
contain

* the ``schedule`` list containing an initial schedule (i.e. initially
  planned departure times),
* the ``block_times`` list containing the duration of each leg,
* the ``env`` dictionnary, whose purpose will be developped later,
* the ``constraints`` string.

Constraints grammar
  The constraints are specified in the `constraints` string following
  the grammar,

  ::

     constraints  ::= constraints \n constraint
                    | constraint
     constraint   ::= lhexpression <= rhexpression
     lhexpression ::= lhexpresssion + lhterm
                    | lhexpression - lhterm
                    | lhterm
     lhterm       ::= factor * metavar
                    | metavar
     metavar      ::= { INT }
     rhexpression ::= rhexpression + rhexpression
                    | rhexpression - rhexpression
		    | rhexpression * rhexpression
		    | rhexpression / rhexpression
		    | rhterm
     rhterm       ::= factor
                    | var
     factor       ::= FLOAT
                    | ~- FLOAT
     var          ::= IDENT [ INT ]
                    | IDENT

  The arithmetic operators stand for themselves, the ``\n`` is the
  newline character.

  The grammar is indifferent to spaces, each line must contain one
  constraint, inline comments starting with # are allowed.  Do not add
  a newline at the beginning of ``constraints`` nor at the end. Unary
  minus (``~-``) can only be added to factors.  Factors must be float
  and have a dot.

  The decision variables of the problem (i.e. the shifts from the
  original schedule, the :math:``x`` in the model above) are embodied
  by the ``metavar`` nonterminals in the grammar. All shift
  specification must thus be at the *left* of the ``<=``, and the
  constant at the right. Variables may be used in the definition of
  constraints as long as the name are provided in the ``env``
  dictionary. Variables might be atomic or indexed lists.
