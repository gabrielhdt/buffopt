.. Buffopt documentation master file, created by
   sphinx-quickstart on Mon Jun 11 21:19:21 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Buffopt's documentation
===================================


Airlines are subject to a particular trade off: they must, as it is their duty,
be able to carry people from one point to another at scheduled time without
delay and to avoid losing money, they have to use each aircraft as much as
possible and thus minimise the time each aircraft spend on the ground. The
latter proposition entices airlines to make tight schedules. However as
schedules are subject to delay, making them too tight would jeopardise the
fulfillment of the first proposition in case of unexpected events.

This trade off may be solved using buffers, i.e. allowing extra time to each
operation in the schedule to compensate any delay. Those buffers may be chosen
via optimisation techniques.

This project aims to define those buffers with genetic algorithms. The work is
based on the article *A multi objective genetic algorithm for robust flight
scheduling using simulation* by Lee et.\ al.\ published on January 23, 2006 at
Elsevier, European Journal of Operational Research.


Litterature review
------------------
The aim of the schedule is to manage potential delays. One has thus to be able
to take delays into account.

Contents:

.. toctree::
   :maxdepth: 2

   solver
   tactical_agent
   simulator
   constraints
   source/modules

How to use it
=============

Download the project

.. code:: shell

	  git clone https://gitlab.com/gabrielhdt/buffopt

Strategic manager
-----------------
Write a configuration file ``data.py`` as described in the
:ref:`constraints-input` section, or copy ``data.py.sample`` to ``data.py``.

Launch the script

.. code:: shell

	  ./optimiser.py --population p --ngen n --alpha a

which will optimise the schedule given in ``data.py`` thanks to a population
of ``p`` chromosomes having evolved through ``n`` generation. The ``a``
parameter affects th fitness function. The closer ``a`` is to 1, the more the
algorithm will avoid having delays.

Tactical manager
----------------
Not yet implemented


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

