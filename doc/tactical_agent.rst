Tactical manager
================

Description
-----------
The aim of the tactical manager is to optimise a schedule in real time,
i.e. to modify a flight based on what happen on all flights before in
the day in order to reduce costs.

Algorithm
---------
The tactical manager is based on a Q learning algorithm. It means that for any
new schedule, the agent needs to be trained.

Model

  * A state of the environment is composed of the current delay and the number
    of the leg to be flown.
  * A rectricted amount of actions are feasible. The currently to-be implemented
    actions are rerouting and speeding up the next flight.
