[![pipeline status](https://gitlab.com/gabrielhdt/buffopt/badges/master/pipeline.svg)](https://gitlab.com/gabrielhdt/buffopt/commits/master)
[![coverage report](https://gitlab.com/gabrielhdt/buffopt/badges/master/coverage.svg)](https://gitlab.com/gabrielhdt/buffopt/commits/master)

# Airline strategic scheduler

## Description
The role of the strategic manager is to optimise a given schedule by applying
shifts to it. The program outputs a list of shifts to be applied to the given
schedule.

## How to use
The solver takes its constraints from a file named `data.py` which must be
at the root of the project. This file must contain
* an initial schedule in a variable `schedule` (list of int)
* the block times of each flight in a variable `block_times` (list of float)
* a string containing constraints following an ad-hoc grammar.

The grammar can be found in the documentation
([[https://gabrielhdt.gitlab.io/buffopt/constraints.html#constraints-input]]).

Once this is created, the optimiser may be started
```
$ ./optimiser.py --population p --elite e --ngen g --alpha a
```
where `p` is the size of the population, `e` the size of the elite
population, `g` the number of generations used and `alpha` a parameter
for the finess function.

## Fitness function
The fitness function aims currently at minimising the amount of delay
encountered during a day and reduce the total time of the schedule
(i.e. reduce time between first and last departures)


## Tests
In `test` are several unit tests. They may be launched using
```
$ tox
```

## Todo
- [x] normalise management of fitness
- [ ] constraint solutions of schedule to rounded times
- [ ] better management of data file
- [ ] complete doc
- [ ] improve constraints grammar
- [ ] lock departure time
- [ ] use a nominal use cost to keep costs positive (needed for roulette
      wheel)
