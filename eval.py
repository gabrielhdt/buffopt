#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model

import buffopt.strategic.geneticsolver as GeneticSolver
import buffopt.strategic.schedule as Schedule

if os.path.exists("data.py"):
    import data
else:
    print("data.py missing")
    sys.exit(-1)


def main(args):
    schedule = Schedule.make([int(s) for s in data.schedule],
                             [int(b) for b in data.block_times],
                             data.constraints, data.env)
    sched_check = Schedule.check(schedule)
    if not sched_check[0]:
        raise ValueError("input schedule not consistent")

    if args.evaluate == "generations":
        pop = GeneticSolver.make(args.population, schedule, alpha=args.alpha)
        elite = GeneticSolver.make_elite(pop, schedule, alpha=args.alpha)
        el_rslt = []
        pl_rslt = []
        for _ in range(args.ngen):
            pop, elite = GeneticSolver.evolve(schedule, pop, elite, args.alpha)
            el_best = GeneticSolver.best(elite)
            pl_best = GeneticSolver.best(pop)
            el_rslt.append(el_best[1])
            pl_rslt.append(pl_best[1])

        x_data = np.array(list(range(args.ngen))).reshape(-1, 1)
        genreg = linear_model.LinearRegression()
        genreg.fit(x_data, el_rslt)

        with open(args.outfile + ".fit", 'w+') as f:
            for elite_elt, plebeian_elt in zip(el_rslt, pl_rslt):
                f.write("{} {}\n".format(elite_elt, plebeian_elt))
        print(genreg.coef_)
        plt.plot(x_data, el_rslt)
        plt.plot(x_data, pl_rslt)
        plt.plot(x_data, genreg.predict(x_data))
        plt.show()

    elif args.evaluate == "pop_size":
        el_rslt, pl_rslt = [], []
        for psize in range(40, args.population, 10):
            print("{}/{}".format(psize, args.population), end="\r")
            pop = GeneticSolver.make(psize, schedule, alpha=args.alpha)
            elite = GeneticSolver.make_elite(pop, schedule, alpha=args.alpha)
            for _ in range(args.ngen):
                pop, elite = GeneticSolver.evolve(schedule, pop, elite,
                                                  args.alpha)
            el_rslt.append(GeneticSolver.best(elite)[1])
            pl_rslt.append(GeneticSolver.best(pop)[1])

        x_data = np.array(list(range(40, args.population, 10))).reshape(-1, 1)
        popreg = linear_model.LinearRegression()
        popreg.fit(x_data, el_rslt)

        plt.plot(list(range(40, args.population, 10)), pl_rslt)
        plt.plot(list(range(40, args.population, 10)), el_rslt)
        plt.plot(x_data, popreg.predict(x_data))
        plt.show()
    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Evaluate genetic solver")
    parser.add_argument(
        "evaluate",
        choices=["pop_size", "generations"],
        help=("Choose to test influence of number of generations "
              "or size of population"))
    parser.add_argument(
        '--population',
        type=int,
        metavar="P",
        dest="population",
        required=True,
        help="Size of the population")
    parser.add_argument("--ngen", dest="ngen", type=int)
    parser.add_argument("--outfile", dest="outfile", type=str, default="out")
    parser.add_argument("--alpha", dest="alpha", type=float, default=0.8)
    gargs = parser.parse_args()

    main(gargs)
