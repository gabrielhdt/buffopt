# -*- coding: utf-8 -*-
from functools import reduce
from typing import Any, Callable, Dict, List, NamedTuple, Tuple

import numpy  # type: ignore

import buffopt.constraintparse.lexer as lexer
import buffopt.constraintparse.parser as parser
from buffopt.constraintparse.cstTypes import (ConstraintEq,
                                              Constraints,
                                              VarSpec)

T = NamedTuple("T", [("timetable", List[int]), ("block_times", List[int]),
                     ("constraints", Constraints), ("mtow", int),
                     ("scenario", str)])


def _parse_constraints(s: str, env: Dict) -> Constraints:
    """Parses constraints contained in string s.

    Args:
        s (str): string containing the constraint; the grammar of constraint
                 is specified in the doc (hopefully)
        env: an optional environment which might be needed to parse the
             constraints, if the constraints call some variables

    Returns:
        the resulting constraints structure
    """
    lex = lexer.gen_lexer(env)
    return parser.parse(s, lex)


def make(timetable: List[int],
         block_times: List[int],
         constraints: str,
         mtow: int, scenario: str,
         env: Dict[str, Any] = {}) -> T:
    """Creates a schedule structure.

    Args:
        timetable: the initial timetable to be improved
        block_times: duration of each leg
        constraints: constraints on the new timetable, written as
                     equations, one equation per list element
        mtow: max take off weight of aircraft
        scenario: the supportiveness foreseen
        env: an additional environment containing variables that should be
             evaluated during parsing

    Returns:
        the complete structure ready to be used
    """
    parsed_cons = _parse_constraints(constraints, {
        "timetable": timetable,
        "block_times": block_times,
        **env
    })
    return T(timetable=timetable, block_times=block_times, mtow=mtow,
             scenario=scenario[0], constraints=parsed_cons)


def card(schedule: T) -> int:
    """Returns the number of legs flown."""
    return len(schedule.block_times)


def get_start(schedule: T) -> int:
    """Gets the start time of the schedule"""
    return schedule.timetable[0]


def max_buffer_reduction(s: T) -> int:
    mtbnds = [min_time_before_next_dep(s, x)
              for x in range(1, card(s))]
    diffdep = [s.timetable[x] - s.timetable[x - 1] for x in range(1, card(s))]
    return max([mtbnd - dd for mtbnd, dd in zip(mtbnds, diffdep)])


def min_time_before_next_dep(schedule: T, legno: int) -> int:
    """Gives the minimum time needed of a leg.

    The value is deduced from the constraints. If a constraint involves two
    consecutive ( ?) flights with opposite signs, then the constant associated
    is the value we want here. For instance, the constraint
    {2} - {3} <= ~-1. * block_times[2] - t_p_conn
    involves vars 2 and 3 having opposite signs with {2} being positive,
    it therefore involves time after constraint {2}. The minimum time in this
    constraint is block_times[2] + t_p_conn. Four assertions are verified

    * the constraint contains the variable having number `legno`,
    * the coefficient of this variable is positive,
    * there is only one other variable involved,
    * the coefficient of the other variable is negative.

    Args:
        schedule: the full schedule
        legno: the leg number whose needed time is computed

    Returns:
        the duration of leg `legno` such that the next departure cannot be
        fixed before this value, the next departure time will thus be either
        the scheduled time if there is no delay, or the departure time plus the
        minimum time needed plus an additional delay encountered during the
        leg.
    """

    def keep_cstr(constraint: ConstraintEq):
        with_legno = reduce(lambda acc, vs: (vs.index == legno and
                                             vs.coeff >= 0) or acc,
                            constraint.vars, False)
        other_vars = [v for v in constraint.vars if v.index != legno]
        return with_legno and len(other_vars) == 1 and other_vars[0].coeff <= 0

    keptconsts = [c for c in schedule.constraints if keep_cstr(c)]
    return max([abs(c.constant) for c in keptconsts])


def vslookup(ind: int, xs: List[VarSpec]):
    if len(xs) == 0:
        raise KeyError
    else:
        if xs[0].index == ind:
            return xs[0].coeff
        else:
            return vslookup(ind, xs[1:])


def _buildlinfuns(ceq: ConstraintEq, n: int) -> Callable[[List[int]], int]:
    """Given a constraint Ax <= b, builds the function x -> Ax.

    Args:
        ceq: the constraint equation of the form Ax <= b with b in R and A a
             line vector of dimension the number of decision variables
        n: the number of decision variables

    Returns:
        the linear application x -> Ax
    """
    vinds = [vs.index for vs in ceq.vars]
    a = [vslookup(i, ceq.vars) if i in vinds else 0 for i in range(n)]
    return (
        lambda x: reduce(lambda acc, elt: acc + elt[0] * elt[1], zip(a, x), 0))


def check(schedule: T) -> Tuple[bool, List[int]]:
    """Checks if the given schedule is consistent.

    Args:
        schedule: the schedule

    Returns:
        the boolean indicates whether the schedule is consistent or not; if
        not, each inconsistent constraint is indicated in the list
    """
    afuns = [
        _buildlinfuns(ceq, card(schedule)) for ceq in schedule.constraints
    ]
    bs = [ceq.constant for ceq in schedule.constraints]
    # In theory, it should be <= but since we end up dividing by a(s) - b,
    # we better not have a(s) = b
    consistent = [a(schedule.timetable) < b for a, b in zip(afuns, bs)]
    init = []  # type: List[int]
    inconsind = reduce(lambda acc, x: [x[0]] + acc if not x[1] else acc,
                       enumerate(consistent), init)
    return (len(inconsind) == 0, inconsind)


def correct(schedule: T, shifts: List[int]) -> List[int]:
    """Checks if the buffers comply to the constraints.

    Args:
        schedule: the times originally planned
        legs: time of legs
        shifts: shifted departure times

    Returns:
        the corrected list of shifts
    """

    amats = [
        _buildlinfuns(ceq, card(schedule)) for ceq in schedule.constraints
    ]
    bs = [ceq.constant for ceq in schedule.constraints]
    # Remember that b - a(schedule) is positive
    violations = [  # Indexes of the violated constraints
        i for i, (a, b) in enumerate(zip(amats, bs))
        if a(shifts) > b - a(schedule.timetable)
    ]
    if len(violations) > 0:
        # Actually the index of the most violating among the violating
        # constraints
        mvcav = numpy.argmax([
            amats[i](shifts) / (bs[i] - amats[i](schedule.timetable))
            for i in violations
        ])
        mvc = violations[mvcav]
        return [
            int(e * (bs[mvc] - amats[mvc](schedule.timetable)) /
                amats[mvc](shifts)) for e in shifts
        ]
    else:
        return shifts


def delays(schedule: T, shifts: List[int], departures: List[int]) -> List[int]:
    """Computes delays in the new departure times."""
    return [
        a - (e + s) for a, e, s in zip(departures, schedule.timetable, shifts)
    ]


def timespan(schedule: T) -> int:
    """Returns the timespan of the schedule"""
    return schedule.timetable[-1] - schedule.timetable[0]


def _inter_extra(keys, value):
    if value < min(keys):
        return (min(keys), min([k for k in keys if k > min(keys)]))
    elif value > max(keys):
        return (max([k for k in keys if k < max(keys)]), max(keys))
    else:
        return (max([k for k in keys if k <= value]),
                min([k for k in keys if k > value]))


def delay_cost(s: T, delay: int) -> int:
    """Returns the cost of delay

    Data comes from a table. The dictionnary here maps mtow to non reactionary
    cost for (5, 15, 30, 60, 90, 120, 180, 240, 300) minutes of delay.
    """
    minutes = (5, 15, 30, 60, 90, 120, 180, 240, 300)
    delay_data = {
        65000: (70, 340, 1010, 3250, 6350, 10140, 19700, 31820, 46350),
        # 68000: (80, 370, 1110, 3630, 7150, 11440, 22330, 36140, 52710),
        60000: (70, 310, 920, 2930, 5720, 9120, 17700, 28570, 41590),
        79000: (80, 410, 1230, 4010, 7910, 12670, 24740, 40070, 58440),
        116000: (90, 470, 1460, 4850, 9620, 15460, 40290, 49150,
                 71810),
        159000: (130, 690, 2150, 7170, 14240, 22900, 44920, 72920,
                 106570),
        412770: (190, 970, 3050, 10240, 20400, 32850, 64520, 104840,
                 153310),
        64000: (70, 340, 1020, 3320, 6540, 10480, 20450, 33110, 48300),
        68000: (80, 380, 1150, 3770, 7460, 11960, 23390, 37900, 55330),
        83000: (80, 430, 1350, 4490, 8920, 14360, 28170, 45730, 66850),
        18600: (40, 160, 430, 1260, 3290, 3750, 7140, 11400, 16460),
        22800: (50, 190, 540, 1660, 3200, 5060, 9720, 15580, 22590)
    }

    # Find closest keys to mtow
    below_m, above_m = _inter_extra(delay_data.keys(), s.mtow)

    # Find closest timesteps
    below_t, above_t = _inter_extra(minutes, delay)
    below_ti, above_ti = minutes.index(below_t), minutes.index(above_t)
    return int(
        (delay_data[above_m][above_ti] - delay_data[below_m][below_ti]) /
        (above_t - below_t) * (delay - below_t) + delay_data[below_m][below_ti]
    )


def buffer_cost(s: T, shifts: List[int]) -> int:
    """Returns the cost of shifts (can be a reward)

    The table given contains the cost per hour of buffer for each
    scenario
    """
    buffer_data = {
        65000: {"l": 340, "m": 740, "h": 1260},
        68000: {"l": 370, "m": 800, "h": 1380},
        60000: {"l": 340, "m": 760, "h": 1310},
        79000: {"l": 440, "m": 1020, "h": 1790},
        116000: {"l": 540, "m": 1100, "h": 1850},
        159000: {"l": 740, "m": 1550, "h": 2810},
        412770: {"l": 1070, "m": 2180, "h": 3830},
        64000: {"l": 430, "m": 930, "h": 1570},
        # 68000: {"l": 480, "m": 1070, "h": 1830},
        83000: {"l": 550, "m": 1210, "h": 2050},
        18600: {"l": 180, "m": 360, "h": 620},
        22800: {"l": 220, "m": 470, "h": 800}
    }
    below_m, above_m = _inter_extra(buffer_data.keys(), s.mtow)

    approx_rate = (
        (buffer_data[above_m][s.scenario] - buffer_data[below_m][s.scenario]) /
        (above_m - below_m) *
        (s.mtow - below_m) + buffer_data[below_m][s.scenario]
    )
    return int(reduce(lambda acc, shift: acc + approx_rate * shift / 60,
                      shifts, 0.))
