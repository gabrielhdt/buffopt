from functools import reduce
from typing import List

from buffopt.proba_distrib import non_reactionary_delay
from buffopt import schedule

_max_delay = 24 * 60
"""Maximum delay achievable"""


def _simblock(last_departure: int, legno: int, sched: schedule.T,
              shifts: List[int]) -> int:
    """Computes the next time of departure.

    Args:
        last_departure: time of the last departure (in minutes)
        legno: the number of the leg
        schedule: various schedule data
        shifts: the shifts applied to original schedule

    Returns:
        the next time of departure
    """
    assert legno < schedule.card(sched) - 1
    delay = non_reactionary_delay()
    timeneeded = schedule.min_time_before_next_dep(sched, legno)
    expected_next_departure = sched.timetable[legno + 1] + shifts[legno + 1]
    nextdeparture = max(last_departure + timeneeded + delay,
                        expected_next_departure)
    return nextdeparture


def run(sched: schedule.T, shifts: List[int]) -> List[int]:
    """Simulation of the encounterered delays.

    Simulates the delay accumulated along a day for an aircraft. This delay
    can be reduce by the use of buffers.

    Args:
        legs (List[Leg]): the several legs the aircraft has to perform
        buffers (List[int]): buffers used to compensate probable delays

    Returns:
        the list of experienced departure times
    """

    def _redloop(acc: List[int], ln: int) -> List[int]:
        """Function for foldr.

        Args:
            acc: contains [(departure time, delay so far)]
            ln: leg number

        Returns:
            the same type as acc
        """
        departure = acc[0]
        new_dep = _simblock(departure, ln, sched, shifts)
        return [new_dep] + acc

    first_departure = schedule.get_start(sched) + shifts[0]
    departures_legs = reduce(_redloop,
                             range(schedule.card(sched) - 1),
                             [first_departure])
    return list(reversed(departures_legs))
