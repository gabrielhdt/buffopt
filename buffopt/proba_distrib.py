from scipy.stats import burr12  # type: ignore

non_reac_del_args = (18.86, 0.57)
non_reac_del_kwargs = {"scale": 69.82}
non_reac_del_shift = -70
"""Parameters of non reactionary burr distribution"""


def non_reactionary_delay() -> int:
    return max(0, int(non_reac_del_shift
                      + burr12.rvs(*non_reac_del_args,
                                   **non_reac_del_kwargs)))
