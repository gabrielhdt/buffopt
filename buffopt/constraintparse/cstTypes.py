"""Contains types shared between the parser and constSchedule.

Has been done to avoid cyclic dependencies
"""
from typing import List, NamedTuple

VarSpec = NamedTuple("VarSpec", [("index", int), ("coeff", float)])
ConstraintEq = NamedTuple("ConstraintEq", [("vars", List[VarSpec]),
                                           ("constant", int)])
Constraints = List[ConstraintEq]


def change_sign(v: VarSpec) -> VarSpec:
    """Changes the sign of the coefficient.

    Args:
        v: variables whose sign must be changed

    Returns:
        another variable with sign changed
    """
    return VarSpec(v.index, -v.coeff)
