"""Parser for constraints"""
from ply import yacc  # type: ignore

from buffopt.constraintparse.cstTypes import (ConstraintEq,
                                              Constraints,
                                              VarSpec,
                                              change_sign)
from buffopt.constraintparse.lexer import tokens  # noqa: F401

precedence = (
    ("left", "PLUS", "MINUS"),
    ("left", "TIMES", "DIV"),
)


def p_constraints_sc(p):
    """constraints : constraints NL constraint"""
    p[0] = p[1] + [p[3]]


def p_constraints(p):
    """constraints : constraint"""
    p[0] = [p[1]]


def p_constraint(p):
    """constraint : lhexpression LEQ rhexpression"""
    p[0] = ConstraintEq(p[1], int(p[3]))


def p_lhexpression_plus(p):
    """lhexpression : lhexpression PLUS lhterm"""
    p[0] = p[1] + [p[3]]


def p_lhexpression_minus(p):
    """lhexpression : lhexpression MINUS lhterm"""
    p[0] = p[1] + [change_sign(p[3])]


def p_lhexpression_lht(p):
    """lhexpression : lhterm"""
    p[0] = [p[1]]


def p_lhterm_times(p):
    """lhterm : factor TIMES metavar"""
    p[0] = VarSpec(p[3], p[1])


def p_lhterm_meta(p):
    """lhterm : metavar"""
    p[0] = VarSpec(p[1], 1.)


def p_metavar(p):
    """metavar : LCURLBR INT RCURLBR"""
    p[0] = p[2]


def p_rhexpression_op(p):
    """rhexpression : rhexpression PLUS rhexpression
                    | rhexpression MINUS rhexpression
                    | rhexpression TIMES rhexpression
                    | rhexpression DIV rhexpression"""
    if p[2] == "+":
        p[0] = p[1] + p[3]
    elif p[2] == "*":
        p[0] = p[1] * p[3]
    elif p[2] == "-":
        p[0] = p[1] - p[3]
    elif p[2] == "/":
        p[0] = p[1] / p[3]


def p_rhexpression(p):
    """rhexpression : rhterm"""
    p[0] = p[1]


def p_rhterm_fac(p):
    """rhterm : factor"""
    p[0] = p[1]


def p_rhterm_var(p):
    """rhterm : var"""
    p[0] = p[1]


def p_factor(p):
    """factor : FLOAT"""
    p[0] = p[1]


def p_factor_um(p):
    """factor : UMINUS FLOAT"""
    p[0] = -p[2]


def p_var_ind(p):
    """var : IDENT LBRACKET INT RBRACKET"""
    p[0] = p[1][p[3]]


def p_var(p):
    """var : IDENT"""
    p[0] = p[1]


def p_error(p):
    print("Syntax error in input")


parser = yacc.yacc()


def parse(s: str, lexer) -> Constraints:
    return parser.parse(s, lexer)
