# noqa: F841,F401,N806

from typing import Dict

from ply import lex  # type: ignore

tokens = (
    "NL",
    "INT",
    "FLOAT",
    "IDENT",
    "PLUS",
    "MINUS",
    "DIV",
    "TIMES",
    "LBRACKET",
    "RBRACKET",
    "LCURLBR",
    "RCURLBR",
    "LEQ",
    "UMINUS",
)


def gen_lexer(env: Dict):
    """Generate a constraint lexer.

    Args:
        env: maps names used by the lexer as variables to values

    Returns:
        a lexer
    """
    t_NL = r"\n+"  # noqa: F841,N806
    t_PLUS = r"\+"  # noqa: F841,N806
    t_MINUS = r"-"  # noqa: F841,N806
    t_DIV = r"/"  # noqa: F841,N806
    t_TIMES = r"\*"  # noqa: F841,N806
    t_LBRACKET = r"\["  # noqa: F841,N806
    t_RBRACKET = r"\]"  # noqa: F841,N806
    t_LCURLBR = r"\{"  # noqa: F841,N806
    t_RCURLBR = r"\}"  # noqa: F841,N806
    t_LEQ = r"<="  # noqa: F841,N806
    t_UMINUS = r"~-"  # noqa: F841,N806

    t_ignore = ' \t'  # noqa: F841
    t_ignore_COMMENT = r"\#.*"  # noqa: F841,N806

    def t_IDENT(t):  # noqa: N802
        r"[a-zA-Z_][a-zA-Z_0-9]*"
        t.value = env[t.value]
        return t

    def t_FLOAT(t):  # noqa: N802
        r"(\d+\.\d*|\d*\.\d+)"
        t.value = float(t.value)
        return t

    def t_INT(t):  # noqa: N802
        r"\d+"
        t.value = int(t.value)
        return t

    def t_error(t):
        print("Illegal character {}".format(t.value[0]))
        t.lexer.skip(1)

    return lex.lex()
