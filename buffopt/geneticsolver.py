# -*- coding: utf-8 -*-
from collections import namedtuple
from functools import reduce
from typing import List, Tuple

import numpy.random as rd  # type: ignore

import buffopt.schedule as schedule
import buffopt.simulator as simulator

Gene = int
Chromosome = List[Gene]
T = List[Tuple[Chromosome, float]]

Bounds = namedtuple("Bounds", ["low", "high"])

# Genetic alg parameters
_k_gene_init_bounds = Bounds(-5, 5)
"""Initialisation value for a gene"""
_k_mutate_p = 0.2
"""Probability for a gene to mutate"""
_p_xover = 0.6
"""Probability of crossing a chromosome"""
_p_xover_op = 0.5
"""Probability of choosing the one point xover over arithmetic"""


def make(size: int, sched: schedule.T, sim: int = 3, alpha: float = 0.8) -> T:
    """Create an initial population.

    Args:
        size: size of the population
        param gene_size: size of a gene

    Returns:
        a population of chromosomes of size [gene_size]
    """
    return [(c, fitness(sched, c, sim, alpha)) for c in
            [_rand_chromosome(schedule.card(sched))
             for _ in range(size)]]


def make_elite(size: int, pop: T, sched: schedule.T,
               sim: int = 3 * 20, alpha: float = 0.8) -> T:
    """Extracts an elite population"""
    return sorted(pop, key=lambda e: fitness(
        sched, e[0], sim, alpha))[:size]


def to_list(pop: T) -> List[Chromosome]:
    """Returns the population as a list"""
    return [x[0] for x in pop]


def best(pop: T) -> Tuple[Chromosome, float]:
    """Returns the best element of a population, along with its fitness

    Args:
        pop: the population from which the element is extracted

    Returns:
        the best chromosome and its fitness
    """
    return max(pop, key=lambda x: x[1])


def evolve(sched: schedule.T, population: T, elite: T, mutation: int,
           sim: int = 3, alpha: float = 0.8) -> Tuple[T, T]:
    """Creates the next generation.

    Args:
        sched: all the data available
        population: basis generation
        elite: previous elite population
        mutation: interval allowed for a mutation (mutation in [-m, m])
        alpha: parameter passed to fitness
        sim: the number of simulations carried out

    Returns:
        next generation population and next elite population
    """

    def inner(acc: List[Chromosome]) -> List[Chromosome]:
        """Creates next generation"""
        if len(acc) >= len(population):
            return acc
        else:
            parents = (_roulette_wheel(population),
                       _roulette_wheel(population))
            if _rand_bool(_p_xover):
                if _rand_bool(_p_xover_op):
                    offspring = _one_point_cross(*parents)
                else:
                    child = _arithmetic_cross(*parents)
                    offspring = (child, child)
            else:
                offspring = parents
            children = [_mutate(c, mutation) for c in offspring]
            educated_children = [
                schedule.correct(sched, c) for c in children
            ]
            return inner(educated_children + acc)

    child_pop = inner([])
    childpopfit = [(c, fitness(sched, c, sim, alpha)) for c in child_pop]
    nextelite = [
        (e[0], fitness(sched, e[0], 20 * sim, alpha)) for e in sorted(
            childpopfit, key=lambda e: e[1],
            reverse=True
        )[:len(elite)]
    ]
    return (
        [
            x for x in sorted(
                childpopfit + population, key=lambda cf: cf[1], reverse=True)
        ][:len(population)],
        [
            x for x in sorted(
                elite + nextelite, key=lambda ef: ef[1], reverse=True)
         ][:len(elite)]
        )


def fitness(sched: schedule.T, chrom: Chromosome,
            sim: int, alpha: float) -> float:
    """Fitness function.

    Fitness is based on the count of delays, and the deviation from the
    original schedule

    Args:
        sim: the number of simulations to perform
        alpha: the nearer it is to 1, the higher delays are avoided, on
               the opposite, being near to zero entices the algorithm to
               compact the schedule at the risk of having severe delays
    """
    sim_departures = [simulator.run(sched, chrom)
                      for _ in range(sim)]
    meandeps = [int(sum(sim_departures[i][j] for
                        i in range(sim)) / sim) for j in
                range(len(sim_departures[0]))]
    delays = schedule.delays(sched, chrom, meandeps)
    # total_delay = sum(delays)
    # Sum each delay as delay costs are more precised on low values
    dcost = sum([schedule.delay_cost(sched, d) for d in delays])
    bcost = schedule.buffer_cost(sched, chrom)

    # maxbufred = schedule.max_buffer_reduction(sched)

    return -dcost - bcost

    # reltimespan = ((chrom[-1] - chrom[0]) /
    #                schedule.timespan(sched))
    # return 1 / (1 + alpha * total_delay + (1 - alpha) * (1 + reltimespan))


def _roulette_wheel(chromfit: T) -> Chromosome:
    assert len(chromfit) > 0
    fitsum = reduce(lambda acc, fc: acc + fc[1], chromfit, 0.)
    fitnorm = [(fc[0], fc[1] / fitsum) for fc in chromfit]
    sfitnorm = sorted(fitnorm, key=lambda fc: fc[1])  # Lowest from highest fit

    accfit = [sfitnorm[0]]
    for c, fit in sfitnorm[1:]:
        accfit.append((c, accfit[-1][1] + fit))
    accfit.reverse()

    rd_fl = rd.uniform()
    acc = accfit[0][0]
    for c, fit in accfit:
        acc = c if fit >= rd_fl else acc
    return acc


def _arithmetic_cross(c1: Chromosome, c2: Chromosome) -> Chromosome:
    return [int((g1 + g2) / 2) for (g1, g2) in zip(c1, c2)]


def _one_point_cross(c1: Chromosome,
                     c2: Chromosome) -> Tuple[Chromosome, Chromosome]:
    rd_ind = rd.randint(0, len(c1))
    return (c1[:rd_ind] + c2[rd_ind:], c2[:rd_ind] + c1[rd_ind:])


def _mutate(c: Chromosome, bound: int) -> Chromosome:
    return [
        int(g + rd.uniform(-bound, bound))
        if _rand_bool(_k_mutate_p) else g for g in c
    ]


def _rand_bool(p: float) -> bool:
    return bool(rd.binomial(1, p))


def _rand_chromosome(ngenes: int) -> Chromosome:
    return [
        rd.randint(_k_gene_init_bounds[0], _k_gene_init_bounds[1] + 1)
        for _ in range(ngenes)
    ]
