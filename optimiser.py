#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys
import time

import buffopt.geneticsolver as GeneticSolver
import buffopt.simulator as Simulator
import buffopt.schedule as Schedule

if os.path.exists("data.py"):
    import data
else:
    print("data.py missing")
    sys.exit(-1)


def main(args):

    schedule = Schedule.make([int(s) for s in data.schedule],
                             [int(b) for b in data.block_times],
                             data.constraints, args.mtow, args.scenario,
                             data.env)
    sched_check = Schedule.check(schedule)
    if not sched_check[0]:
        raise ValueError("input schedule not consistent, "
                         "check constraints {}".format(sched_check[1]))
    print("Schedule loaded")
    pop = GeneticSolver.make(args.population, schedule,
                             alpha=args.alpha)
    elite = GeneticSolver.make_elite(args.elite, pop, schedule,
                                     alpha=args.alpha)
    print("Initial populations created")
    if args.time is not None:
        starttime = time.time()
        while time.time() - starttime <= args.time:
            pop, elite = GeneticSolver.evolve(
                schedule, pop, elite, args.mutation, args.sim, args.alpha)
    elif args.ngen is not None:
        for k in range(args.ngen):
            print("Generation {}/{}".format(k, args.ngen), end="\r")
            pop, elite = GeneticSolver.evolve(
                schedule, pop, elite, args.mutation, args.sim, args.alpha)
            best = elite[0]
    simu = Simulator.run(schedule, best[0])
    delays = Schedule.delays(schedule, best[0], simu)
    print("Best shifts: {} with fitness {} and departures {}".format(
        best[0], best[1], simu))
    for l in zip(range(Schedule.card(schedule)), simu, delays):
        print_leg(*l)


def print_leg(legno, actual_departure, delay):
    adfmt = "{}:{}".format(int(actual_departure / 60), actual_departure % 60)
    print("Flight {} departed at {} with a delay of {}min\n".format(
        legno, adfmt, delay))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Optimise buffers")
    parser.add_argument(
        '--population',
        type=int,
        metavar="P",
        dest="population",
        required=True,
        help="Size of the population")
    parser.add_argument(
        "--elite",
        type=int,
        metavar="E",
        dest="elite",
        default=20,
        help="Size of elite population")
    parser.add_argument(
        "--mutation",
        type=int,
        metavar="MUT",
        dest="mutation",
        default=3,
        help="Mutation boundaries")
    parser.add_argument(
        "--sim",
        type=int,
        metavar="S",
        dest="sim",
        default=3,
        help="Number of simulations to carry out for each chromsome")
    parser.add_argument(
        "--time",
        type=float,
        metavar="T",
        dest="time",
        help="Timelapse allocated to the genetic algorithm to run")
    parser.add_argument(
        "--ngen",
        type=int,
        dest="ngen",
        metavar="N",
        help="Number of generations to live through")
    parser.add_argument(
        "--alpha",
        type=float,
        dest="alpha",
        default=0.8,
        metavar="w0",
        help="fitness function param")
    parser.add_argument(
        "--mtow",
        type=int,
        dest="mtow",
        metavar="M",
        required=True,
        help="Max take off weight"
    )
    parser.add_argument(
        "--scenario",
        type=str,
        dest="scenario",
        metavar="SC",
        default="m",
        help="Scenario used, low, medium or high"
    )
    pargs = parser.parse_args()
    main(pargs)
