#!/usr/bin/python3
from setuptools import setup

setup(
    name="buffopt",
    version="0.1b1",
    description="Airline buffers optimiser",
    author="Gabriel Hondet",
    install_requires=[
        "scipy",
        "numpy",
        "ply",
    ])
