#+title: Strategic scheduling optimisation by genetic algorithms
#+author: Gabriel Hondet
#+options: tex:t
#+bibliography: strategic plain

* Introduction
   Blablabla

* Literature review
  The aim of a schedule is to be robust enough to delays.  One has
  thus to be able to take delays into account. In~\cite{Lee2006moga},
  delays are experienced through as simulator (SIMAIR).  As a
  schedule is fed into the simulator, the latter carries out airline
  operations simulations, including anomalies and unexpected events.
  The output of the simulation is then used into a cost
  function.  Delays can also be considered via a robustness measure,
  as in~\cite{Aloulou2013robustness}.  The robustness measure maps a
  slack time -- an additional time allowed to operations to absorb
  delays -- to a real number in \([0, 1]\).  The closer the
  robustness is to 1, the less delays will impact future flights.
  The mapping used is piece wise linear with a decreasing derivative.
  Even though simulations are not used to determine the solutions,
  they are tested via Monte Carlo simulations.  Delays are generated
  and propagated through the schedules generated to assess their
  robustness.  Delays are sampled from a \(\beta\) distribution.
   
  Schedules must also comply to several constraints.  In the field of
  aviation, many constraints have to be taken into account.  They are
  often related to the crew, e.g.\ in case it must attend a briefing,
  to the aircraft, when for instance a maintenance must be done or
  the passengers.  In~\cite{Lee2006moga} constraints are input as
  hard; each time a candidate is created, either it complies to the
  constraints or it is modified and brought back into the feasible
  domain.  Hard constraints are also implemented
  in~\cite{Aloulou2013robustness}.  In~\cite{burke2009morobust}, the
  constraints are integrated in the cost function; making solution
  violating constraints less prone to be selected.
   
  Once the model is established, a resolution method is needed.
  Several articles such as~\cite{Aloulou2013robustness} use exact
  methods, often, as here, being integer programming.  The other
  option is to use methods involving heuristics via diverse meta
  heuristics such as evolutionary algorithms, used
  in~\cite{Lee2006moga} or~\cite{burke2009morobust}, or particle
  swarm optimisation as in~\cite{jamili2016rmhaiar}.  These meta
  heuristics might be completed by local search methods such as
  simulated annealing, as done in~\cite{jamili2016rmhaiar}.
   
* This work
  In this work, the model will be heavily inspired by the one
  in \cite{Lee2006moga}.  However only one objective will be used.
  + Constraint management will be done the same way, i.e. any
    solution violating a constraint is immediately brought back into
    the feasible domain.  This way, the algorithm can be stopped at
    any time since all solutions are guaranteed to be valid.
  + The algorithm will be genetic with an elite population.  As mentioned
    above, since there is only one objective, the fitness function is
    more pragmatic.

** Fitness function
   The fitness function measures directly the fitness of a
   solution to the problem. The so called fitness is two fold
   1. A schedule is adequate if simulations run on it do not have too much
      delay;
   2. if the time difference between the first and the last departures of the day
      span as less as possible; this way, if they are close enough, a
      new flight might be added to the schedule.

    The second condition ensures that the algorithm does not allocate
    too much time to each flight, which would make the schedule
    inefficient.  The balance between the two conditions is set via a
    coefficient \(\alpha\).

    Let \(\Sigma\) be a schedule, \(S\) the stochastic function
    mapping shifts and a schedule to an amount of delay encountered
    during a simulation, \(\tau\) the function mapping shifts and a
    schedule to the time lapse between the first and the last
    departure. The fitness function is then
    \begin{equation}
	f(c, \Sigma) = \frac{1}{
	    1 + \alpha S(c, \Sigma) + (1 - \alpha) \tau(c, \Sigma)
	    }
    \end{equation}
    
** Delay modelling
   Delays will be integrated into the model via adding a random
   amount of time to any flight. The amount of time is sampled from a
   probability distribution function given in \cite{sesarjue214},
   which is a Burr one.  The pdf used takes into account any delay
   encountered from a departure to the next one, but not delays that
   occurred on previous flights (reactionary delays). The cumulative
   distribution function is given in [[eq:non-reac-delay]].
   #+name: eq:non-reac-delay
   \begin{equation}
       F(x, c, k, l, s) = 1 - \left( 1 + \left( \frac{x - l}{s}\right)^c\right)^{-k}
   \end{equation}
   The parameters used in our case are \(c = 18.86, k = 0.57, l =
   -70, s = 69.82\).
    
* Results
** Experimental setup
   The algorithm has been tested on an Air France aircraft going to
   and fro between Toulouse Blagnac (LFBO) and Orly (LFPO).  The
   schedule has been applied on September 12th, 2014 on an A320
   identified FGHQM. The timetable is given in table [[tab:timetable]].
   The block time between Orly and Blagnac is 1h29.  We suppose
   Blagnac opens at 5:00 a.m.\ and closes at 9:00 p.m.  An aircraft
   turn over lasts 20 minutes, as well as a passenger connection.
   Passenger connections occur at each flight except the first.

#+name: tab:timetable
| Departure time | From apt | To apt | Effective departure |
|----------------+----------+--------+---------------------|
|           5:00 | LFBO     | LFPO   |                5:12 |
|           7:00 | LFPO     | LFBO   |                7:15 |
|           8:50 | LFBO     | LFPO   |                9:03 |
|          10:50 | LFPO     | LFBO   |               11:04 |
|          12:45 | LFBO     | LFPO   |               13:03 |
|          14:45 | LFPO     | LFBO   |               14:58 |
|          16:50 | LFBO     | LFPO   |               16:49 |
|          18:40 | LFPO     | LFBO   |               18:53 |

** Output of program
    The algorithm has been run with a population of 300 chromosomes,
    an elite population of 10, three simulations per chromosome,
    \(\alpha = 0.8\) and 100 generations. The results are displayed
    table [[tab:lfbo-lfpo]]

#+name: tab:lfbo-lfpo
| Shift | Departure time |
|-------+----------------|
|     0 |           5:00 |
|   -11 |           6:49 |
|    -8 |           8:42 |
|   -12 |          10:38 |
|   -14 |          12:31 |
|   -18 |          14:51 |
|   -11 |          16:48 |
|    -9 |          18:37 |

\bibliography{strategic}
\bibliographystyle{plain}
